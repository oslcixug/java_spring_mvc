package com.muigapps.curso.springmvc.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.muigapps.curso.springmvc.project.model.BaseEntity;


@NoRepositoryBean
public interface BaseEntityRepository<E extends BaseEntity, ID> extends JpaRepository<E, ID>, JpaSpecificationExecutor<E> {

}
