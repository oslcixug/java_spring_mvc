package com.muigapps.curso.springmvc.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.muigapps.curso.springmvc.project.manager.ClientManager;
import com.muigapps.curso.springmvc.project.model.Client;

@Controller
@RequestMapping("/clientspecial")
public class ClientSpecialController {
	
	@Autowired
	private ClientManager manager;
	
	@RequestMapping(path = {"/all","/all/"}, produces = {MediaType.TEXT_PLAIN_VALUE})
	public String getAllClient(Model model) {
		
		List<Client> clients = manager.findAll();
		
		model.addAttribute("title", "Listado de clientes");
		model.addAttribute("data", clients);
		
		return "client";
	}
	
	@RequestMapping(path = {"/all2","/all2/"}, produces = {MediaType.TEXT_PLAIN_VALUE})
	public String getAllClient2(Model model) {
		
		List<Client> clients = manager.findAll();
		
		model.addAttribute("title", "Listado de clientes");
		model.addAttribute("data", clients);
		
		return "clientlist";
	}

}
