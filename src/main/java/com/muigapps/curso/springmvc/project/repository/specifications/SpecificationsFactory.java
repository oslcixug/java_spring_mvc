package com.muigapps.curso.springmvc.project.repository.specifications;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterBase;
import com.muigapps.curso.springmvc.project.controller.model.FilterCategory;
import com.muigapps.curso.springmvc.project.controller.model.FilterClient;
import com.muigapps.curso.springmvc.project.controller.model.FilterOrderClient;
import com.muigapps.curso.springmvc.project.controller.model.FilterOrderLine;
import com.muigapps.curso.springmvc.project.controller.model.FilterProduct;
import com.muigapps.curso.springmvc.project.controller.model.FilterStreet;

public class SpecificationsFactory {
	
	public static Specification getSpecification(FilterBase filter) {
		Specification result = null;
		
		if (filter != null) {
			if (filter instanceof FilterOrderClient) {
				return OrderClientSpecifications.getIntsance((FilterOrderClient)filter);
			} else if (filter instanceof FilterOrderLine) {
				return OrderLineSpecifications.getIntsance((FilterOrderLine)filter);
			} else if (filter instanceof FilterProduct) {
				return ProductSpecifications.getIntsance((FilterProduct)filter);
			} else if (filter instanceof FilterStreet) {
				return StreetSpecifications.getIntsance((FilterStreet)filter);
			} else if (filter instanceof FilterCategory) {
				return CategorySpecifications.getIntsance((FilterCategory)filter);
			} else if (filter instanceof FilterClient) {
				return ClientSpecifications.getIntsance((FilterClient)filter);
			}
		}
		

		return result;
	}

}
