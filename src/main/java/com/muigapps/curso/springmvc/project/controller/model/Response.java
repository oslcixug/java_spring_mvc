package com.muigapps.curso.springmvc.project.controller.model;

import javax.persistence.criteria.Order;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.model.Client;
import com.muigapps.curso.springmvc.project.model.OrderClient;
import com.muigapps.curso.springmvc.project.model.OrderLine;
import com.muigapps.curso.springmvc.project.model.Product;
import com.muigapps.curso.springmvc.project.model.Street;

@XmlRootElement(name = "response")
@XmlSeeAlso(value = {
	Client.class,
	Street.class,
	Category.class,
	Product.class,
	OrderLine.class,
	OrderClient.class
})
public class Response<T> {
	
	private Boolean state;
	private int code;
	private String message;
	private T data;
	
	
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	public Response() {
		super();
	}
	public Response(Boolean state, int code, String message, T data) {
		super();
		this.state = state;
		this.code = code;
		this.message = message;
		this.data = data;
	}
	
	
	

}
