package com.muigapps.curso.springmvc.project.repository;

import com.muigapps.curso.springmvc.project.model.OrderLine;

public interface OrderLineRepository extends BaseEntityRepository<OrderLine, Long> {


}
