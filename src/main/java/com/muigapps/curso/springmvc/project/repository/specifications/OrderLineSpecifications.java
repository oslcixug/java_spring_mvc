package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterOrderLine;
import com.muigapps.curso.springmvc.project.model.OrderLine;

public class OrderLineSpecifications implements Specification<OrderLine> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterOrderLine filter;

	public static OrderLineSpecifications getIntsance(FilterOrderLine filter) {
		return new OrderLineSpecifications(filter);
	}

	public OrderLineSpecifications(FilterOrderLine filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<OrderLine> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
