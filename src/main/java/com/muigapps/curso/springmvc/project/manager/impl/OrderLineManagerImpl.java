package com.muigapps.curso.springmvc.project.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.manager.OrderLineManager;
import com.muigapps.curso.springmvc.project.model.OrderLine;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.OrderLineRepository;



@Service
public class OrderLineManagerImpl extends BaseManagerImpl<OrderLine,Long> implements OrderLineManager{

	@Autowired
	private OrderLineRepository repository;

	
	
	@Override
	protected BaseEntityRepository<OrderLine,Long> getRepository() {
		return repository;
	}
	
}
