package com.muigapps.curso.springmvc.project.controller.model;

import java.util.Date;

public class FilterOrderClient extends FilterBase{
	

	private Long idUser;
	private Boolean active;
	private String number;
	private Date from;
	private Date to;
	private Date fromP;
	private Date toP;
	private Date fromD;
	private Date toD;
	
	
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Date getFrom() {
		return from;
	}
	public void setFrom(Date from) {
		this.from = from;
	}
	public Date getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = to;
	}
	public Date getFromP() {
		return fromP;
	}
	public void setFromP(Date fromP) {
		this.fromP = fromP;
	}
	public Date getToP() {
		return toP;
	}
	public void setToP(Date toP) {
		this.toP = toP;
	}
	public Date getFromD() {
		return fromD;
	}
	public void setFromD(Date fromD) {
		this.fromD = fromD;
	}
	public Date getToD() {
		return toD;
	}
	public void setToD(Date toD) {
		this.toD = toD;
	}
	

	

}
