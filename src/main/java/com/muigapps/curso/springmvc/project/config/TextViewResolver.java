package com.muigapps.curso.springmvc.project.config;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

@Component
public class TextViewResolver implements ViewResolver {

	@Autowired
	private TextView textView;
	
	@Autowired
	private TextView2 textView2;
	
	@Override
	public View resolveViewName(String viewName, Locale locale) throws Exception {
		
		if(viewName.equals("client")) {
			return textView;
		} else if(viewName.equals("clientlist")) {
			return textView2;
		}
		
		return textView;
	}

}
