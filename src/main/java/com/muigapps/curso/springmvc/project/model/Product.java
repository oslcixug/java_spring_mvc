package com.muigapps.curso.springmvc.project.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Type;

@Entity
@Table(name="PRODUCT")
@XmlRootElement(name = "product")
public class Product extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty(message = "{product.error.name}")
	private String name;
	@NotNull(message = "{product.error.price}")
	@Min(value = 0l, message = "{product.error.price.min}")
	private Double price;
	private Double priceSuscriber;
	private Double priceToSuscription;
	private Double iva= 21.0d;
	@Lob
	@Type(type = "org.hibernate.type.TextType") 
	private String description;
	private String shortDescription;
	private String imageprin;
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private Category category;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getPriceToSuscription() {
		return priceToSuscription;
	}
	public void setPriceToSuscription(Double priceToSuscription) {
		this.priceToSuscription = priceToSuscription;
	}
	public Double getIva() {
		return iva;
	}
	public void setIva(Double iva) {
		this.iva = iva;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getImageprin() {
		return imageprin;
	}
	public void setImageprin(String imageprin) {
		this.imageprin = imageprin;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}

	public Double getPriceSuscriber() {
		return priceSuscriber;
	}
	public void setPriceSuscriber(Double priceSuscriber) {
		this.priceSuscriber = priceSuscriber;
	}
	
	
	

}
