package com.muigapps.curso.springmvc.project.manager;

import java.util.List;

import com.muigapps.curso.springmvc.project.controller.model.FilterBase;
import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.model.BaseEntity;

public interface BaseManager<T extends BaseEntity,ID> {

	List<T> findAll();
	
	T save(T data) throws CursoSpringMVCException;
	
	T update(ID id, T data) throws CursoSpringMVCException;
	
	T findOne(ID id) throws CursoNotFoundException;
	
	void delete(ID id) throws CursoSpringMVCException;

	ResponseList<T> findPage(Integer page,Integer sizePage);

	ResponseList<T> findPageFilter(Integer page, Integer sizePage, FilterBase filter);
	
	ResponseList<T> findAllFilter(FilterBase filter);
}
