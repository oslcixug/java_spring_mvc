package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterOrderClient;
import com.muigapps.curso.springmvc.project.model.OrderClient;

public class OrderClientSpecifications implements Specification<OrderClient> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterOrderClient filter;

	public static OrderClientSpecifications getIntsance(FilterOrderClient filter) {
		return new OrderClientSpecifications(filter);
	}

	public OrderClientSpecifications(FilterOrderClient filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<OrderClient> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());
		

		if(filter.getIdUser() != null) {
			Join user = root.join("client");
			predicates.add(criteriaBuilder.equal(user.get("id"), filter.getIdUser()));
		}
		if(filter.getActive() != null && filter.getActive()) {
			Join state = root.join("stateOrder");
			predicates.add(criteriaBuilder.notEqual(state.get("id"), 1l));
		}

		

		if(filter.getNumber() != null && !filter.getNumber().isEmpty()) {
			predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("numberOrder")),SpecificationsUtils.prepareSearchString(filter.getNumber())));
			
		}
		

		
		if(filter.getFrom() != null) {
			Calendar from = Calendar.getInstance();
			from.setTime(filter.getFrom());
			from.set(Calendar.HOUR_OF_DAY, 0);
			from.set(Calendar.MINUTE, 0);
			
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createDate"), from.getTime()));
		}
		

		if(filter.getTo() != null) {
			Calendar to = Calendar.getInstance();
			to.setTime(filter.getTo());
			to.set(Calendar.HOUR_OF_DAY, 23);
			to.set(Calendar.MINUTE, 59);
			
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("createDate"), to.getTime()));
		}
		

		
		if(filter.getFromP() != null) {
			Calendar from = Calendar.getInstance();
			from.setTime(filter.getFromP());
			from.set(Calendar.HOUR_OF_DAY, 0);
			from.set(Calendar.MINUTE, 0);
			
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("pickUp"), from.getTime()));
		}
		

		if(filter.getToP() != null) {
			Calendar to = Calendar.getInstance();
			to.setTime(filter.getToP());
			to.set(Calendar.HOUR_OF_DAY, 23);
			to.set(Calendar.MINUTE, 59);
			
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("pickUp"), to.getTime()));
		}
		


		
		if(filter.getFromD() != null) {
			Calendar from = Calendar.getInstance();
			from.setTime(filter.getFromD());
			from.set(Calendar.HOUR_OF_DAY, 0);
			from.set(Calendar.MINUTE, 0);
			
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("delivery"), from.getTime()));
		}
		

		if(filter.getToD() != null) {
			Calendar to = Calendar.getInstance();
			to.setTime(filter.getFromD());
			to.set(Calendar.HOUR_OF_DAY, 23);
			to.set(Calendar.MINUTE, 59);
			
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("delivery"), to.getTime()));
		}
		
		
		
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
