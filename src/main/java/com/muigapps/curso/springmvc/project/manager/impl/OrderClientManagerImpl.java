package com.muigapps.curso.springmvc.project.manager.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.OrderClientManager;
import com.muigapps.curso.springmvc.project.model.Client;
import com.muigapps.curso.springmvc.project.model.OrderClient;
import com.muigapps.curso.springmvc.project.model.OrderLine;
import com.muigapps.curso.springmvc.project.model.Product;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.ClientRepository;
import com.muigapps.curso.springmvc.project.repository.OrderClientRepository;
import com.muigapps.curso.springmvc.project.repository.ProductRepository;
import com.muigapps.curso.springmvc.project.repository.StreetRepository;


@Service
public class OrderClientManagerImpl extends BaseManagerImpl<OrderClient,Long> implements OrderClientManager{

	@Autowired
	private OrderClientRepository repository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private StreetRepository streetRepository;
	@Autowired
	private ClientRepository clientRepository;
	
	
	@Override
	protected BaseEntityRepository<OrderClient,Long> getRepository() {
		return repository;
	}
	
	@Transactional
	@Modifying
	@Override
	public OrderClient save(OrderClient data) throws CursoSpringMVCException {
		data.setPercentTax(21.0);
		
		Client client = clientRepository.findById(data.getClient().getId()).orElse(null);
		if(client == null) {
			throw new CursoSpringMVCException(400, "Se debe indicar el cliente", null);
		}
		data.setClient(client);
			
		if (data.getPickupLocation() == null) {
			throw new CursoSpringMVCException(400, "Se debe indicar la direccion de recogida", null);
		} else {
			if(data.getPickupLocation().getPostalcode() == null) {
				throw new CursoSpringMVCException(400, "Se debe indicar el c\u00F3digo postal de la direccion de recogida", null);
			}

			if(data.getPickupLocation().getId() != null && data.getPickupLocation().getId().longValue() <= 0) {
				data.getPickupLocation().setId(null);
			}
			
			data.getPickupLocation().setClient(data.getClient());
			data.setPickupLocation(streetRepository.save(data.getPickupLocation()));
			data.setDeliveryLocation(data.getPickupLocation());
		}
		
		
		calculateTotal(data);
		

		Integer numAux = repository.nextNumber();
		if (numAux == null) {
			numAux = 1;
		}
		data.setNumberOrder(String.format("%010d", numAux));
		data.setDate(new Date());
		
		
		OrderClient order =  getRepository().save(data);

		
		return order;
	}
	

	@Transactional
	@Override
	public OrderClient update(Long id, OrderClient data) throws CursoSpringMVCException {
		data.setId(id);

		boolean issuc = false;
		
		data.setPercentTax(21.0);
		
		Client client = clientRepository.findById(data.getClient().getId()).orElse(null);
		if(client == null) {
			throw new CursoSpringMVCException(400, "Se debe indicar el cliente", null);
		}
		data.setClient(client);
		
		if (data.getPickupLocation() == null) {
			throw new CursoSpringMVCException(400, "Se debe indicar la direccion de recogida", null);
		} else {
			if(data.getPickupLocation().getPostalcode() == null) {
				throw new CursoSpringMVCException(400, "Se debe indicar el c\u00F3digo postal de la direccion de recogida", null);
			}
			
			if(data.getPickupLocation().getId() != null && data.getPickupLocation().getId().longValue() <= 0) {
				data.getPickupLocation().setId(null);
			}
			data.getPickupLocation().setClient(data.getClient());

			data.setPickupLocation(streetRepository.save(data.getPickupLocation()));
			data.setDeliveryLocation(data.getPickupLocation());
		}
		
		calculateTotal(data);
		
		
		
		OrderClient order =  getRepository().save(data);

		
		
		return order;
	}
	
	private void calculateTotal(OrderClient data) throws CursoSpringMVCException {
		
		if(data != null && data.getLines() != null && !data.getLines().isEmpty()) {
			data.setTotal(0d);
			data.setBase(0d);
			data.setDiscount(0d);
			for(OrderLine line: data.getLines()) {
				if(line.getProduct() != null && line.getProduct().getId() != null) {
					Product p = productRepository.findById(line.getProduct().getId()).orElse(null);
					line.setProduct(p);
				}
			  
			}
			for(OrderLine line: data.getLines()) {
				line.setOrderclient(data);
				if(line.getId() != null && line.getId().longValue() <= 0) {
					line.setId(null);
				}
				if(line.getProduct() != null && line.getProduct().getId() != null) {
					 line.setPrice(line.getProduct().getPrice());
					 if(line.getAmount() == null) {
						 line.setAmount(1d);
					 }
					 if(line.getDiscountPercent() != null && line.getDiscountPercent().doubleValue() > 0d) {
						 line.setDiscount((line.getDiscountPercent() / 100d ) * line.getBase());
						 line.setTotal(line.getBase() - line.getDiscount());
						
						 data.setBase(data.getBase() + (line.getTotal() / (1+(data.getPercentTax() / 100))));
						 data.setDiscount(data.getDiscount() + (line.getDiscount() != null? (line.getDiscount() / (1 + (data.getPercentTax() / 100)) ):0d));
						 data.setTotal(data.getTotal() + (line.getTotal() != null?line.getTotal():0d));
						 
					 } else {
						 line.setTotal(line.getAmount() * line.getPrice());
						 data.setBase(data.getBase() + (line.getTotal() / (1+(data.getPercentTax() / 100))));
						 data.setTotal(data.getTotal() + (line.getTotal() != null?line.getTotal():0d));
					 }				
				 }
			}

			data.setTax(data.getTotal() - ( data.getTotal() / 1.21 ));
			if(data.getDiscountPercent() != null ) {
				data.setDiscount(data.getBase() * data.getDiscountPercent() / 100d);
				data.setBase(data.getBase() - data.getDiscount());
				data.setTax(data.getBase() * (data.getPercentTax() / 100));
			} else {
				data.setDiscount(0d);
			}
			data.setTotal(data.getBase() + data.getTax());
			
		} else {
			throw new CursoSpringMVCException(400, "Debe tener al menos una linea de pedido", null);
		}
	}
	
	
}
