package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterStreet;
import com.muigapps.curso.springmvc.project.model.Street;

public class StreetSpecifications implements Specification<Street> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterStreet filter;

	public static StreetSpecifications getIntsance(FilterStreet filter) {
		return new StreetSpecifications(filter);
	}

	public StreetSpecifications(FilterStreet filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<Street> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());

		if(filter.getIdUser() != null) {
			Join user = root.join("client");
			predicates.add(criteriaBuilder.equal(user.get("id"), filter.getIdUser()));
		}
		
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
