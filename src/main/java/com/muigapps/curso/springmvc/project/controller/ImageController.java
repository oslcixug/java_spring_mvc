package com.muigapps.curso.springmvc.project.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.muigapps.curso.springmvc.project.controller.model.Response;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;

@RestController
@RequestMapping("/image")
public class ImageController {

	@Value("${folder.uploads}")
	private String folder;

	@RequestMapping(path = { "/upload", "/upload/" }, method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Response<String> singleFileUpload(@RequestParam("fichero") MultipartFile file) throws CursoSpringMVCException {
		if (file.isEmpty()) {
			throw new CursoSpringMVCException(400, "No se ha recibido el archivo", null);
		}
		try {
			String fileName = file.getOriginalFilename();
			String parts[] = fileName.split("\\.");
			String ext = parts[parts.length - 1];
			fileName = Calendar.getInstance().getTimeInMillis() + "." + ext;

			InputStream is = file.getInputStream();

			Files.copy(is, Paths.get(folder + fileName), StandardCopyOption.REPLACE_EXISTING);

			Response<String> response = new Response<>();
			response.setCode(200);
			response.setState(true);
			response.setData(fileName);
			return response;
		} catch (IOException e) {
			throw new CursoSpringMVCException(400, "No se ha recibido el archivo", e);
		}
	}

	@RequestMapping(path = { "/{image}" }, method = RequestMethod.GET)
	public void getImageAsByteArray(@PathVariable("image") String name, HttpServletResponse response) throws IOException {
		try {
			InputStream in = new FileInputStream(new File(folder + name));
			String headerValue = CacheControl.maxAge(120, TimeUnit.DAYS).getHeaderValue();
			response.addHeader("Cache-Control", headerValue);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			
			IOUtils.copy(in, response.getOutputStream());
			
		} catch (Exception e) {
			response.setStatus(404);
		}
	}
	
	

}
